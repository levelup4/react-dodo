import axios from "axios";

export const fetchPizzas = (category, sortBy) => (dispatch) => {
  dispatch(setLoaded(false));
  axios
    .get(
      `http://localhost:4000/pizzas?${
        category !== null ? "category=" + category : ""
      }&_sort=${sortBy}&_order=desc`
    )
    .then(({ data }) => {
      dispatch(setPizzas(data));
      dispatch(setLoaded(true));
    });
};

export const setLoaded = (payload) => ({
  type: "SET_LOADED",
  payload
});

// Action creator
export const setPizzas = (items) => ({
  type: "SET_PIZZAS",
  payload: items
});
