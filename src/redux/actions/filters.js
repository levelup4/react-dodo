export const setSortBy = (sortType) => ({
  type: "SET_SORT_BY",
  payload: sortType
});

export const setCategory = (idx) => ({
  type: "SET_CATEGORY",
  payload: idx
});
