import React, { useCallback, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import { Categories, PizzaBlock, SortPopup } from "../components";
import { setCategory, setSortBy } from "@redux/actions/filters";
import { fetchPizzas } from "@redux/actions/pizzas";
import { addPizzaToCart } from "@redux/actions/cart";

const categoryNames = [
  "Мясные",
  "Вегетарианская",
  "Гриль",
  "Острые",
  "Закрытые"
];

const sortItems = [
  { title: "популярности", type: "rating" },
  { title: "цене", type: "price" },
  { title: "алфавиту", type: "name" }
];

function Home() {
  const dispatch = useDispatch();
  const { items, isLoaded } = useSelector(({ pizzas }) => pizzas);
  const { category, sortBy } = useSelector(({ filters }) => filters);
  const cartItems = useSelector(({ cart }) => cart.items);

  useEffect(() => {
    dispatch(fetchPizzas(category, sortBy));
  }, [dispatch, category, sortBy]);

  const onSelectCategory = useCallback(
    (idx) => {
      dispatch(setCategory(idx));
    },
    [dispatch]
  );

  const onSelectSortBy = useCallback(
    (idx) => {
      dispatch(setSortBy(sortItems[idx].type));
    },
    [dispatch]
  );

  const onAddToCart = (pizzaObj) => {
    dispatch(addPizzaToCart(pizzaObj));
  };

  return (
    <div className="container">
      <div className="content__top">
        <Categories items={categoryNames} onClickItem={onSelectCategory} />
        <SortPopup items={sortItems} onClickItem={onSelectSortBy} />
      </div>
      <h2 className="content__title">Все пиццы</h2>
      <div className="content__items">
        {isLoaded
          ? items.map((item) => (
              <PizzaBlock
                key={item.id}
                {...item}
                onAddToCart={onAddToCart}
                quantity={cartItems[item.id]?.length}
              />
            ))
          : Array(10)
              .fill(0)
              .map((_, idx) => <PizzaBlock key={idx} isLoading={!isLoaded} />)}
      </div>
    </div>
  );
}

export default Home;
