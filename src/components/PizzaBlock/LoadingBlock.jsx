import React from "react";
import ContentLoader from "react-content-loader";

function LoadingBlock() {
  return (
    <ContentLoader
      speed={2}
      width={280}
      height={450}
      viewBox="0 0 280 517"
      backgroundColor="#f3f3f3"
      foregroundColor="#ecebeb"
    >
      <rect x="0" y="260" rx="6" ry="6" width="280" height="24" />
      <rect x="0" y="304" rx="6" ry="6" width="280" height="84" />
      <rect x="0" y="413" rx="6" ry="6" width="90" height="27" />
      <rect x="128" y="408" rx="25" ry="25" width="152" height="44" />
      <circle cx="135" cy="125" r="125" />
    </ContentLoader>
  );
}

export default LoadingBlock;
