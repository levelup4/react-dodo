import React, { useState } from "react";
import classNames from "classnames";

const Categories = React.memo(({ items, onClickItem }) => {
  const [selectedItem, setSelectedItem] = useState(null);

  const onSelectItem = (idx) => {
    setSelectedItem(idx);
    onClickItem(idx);
  };

  return (
    <div className="categories">
      <ul>
        <li
          className={classNames({
            active: selectedItem === null
          })}
          onClick={() => onSelectItem(null)}
        >
          Все
        </li>
        {items.map((item, idx) => (
          <li
            key={`${item}_${idx}`}
            onClick={() => onSelectItem(idx)}
            className={classNames({
              active: selectedItem === idx
            })}
          >
            {item}
          </li>
        ))}
      </ul>
    </div>
  );
});

export default Categories;
